/*
������� ����� Animal � ��������� ������� Voice() ������� ������� � ������� ������ � �������.
    
����������� �� Animal ������� ��� ������ (� ������� Dog, Cat � �.�.) 
� � ��� ����������� ����� Voice() ����� �������, 
����� ��� ������� � ������ Dog ����� Voice() ������� � ������� "Woof!".

� ������� main ������� ������ ���������� ���� Animal 
� ��������� ���� ������ ��������� ��������� �������.
    
����� �������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice().
�������������� ��� ������: ������ ���������� ��������� �� ����� ������� ����������� Animal.
*/

#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "This method provides my speech.";
    }
    virtual ~Animal() {}
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

int main()
{
    const int animalsNum = 3;
    Animal* animals[animalsNum];
    
    animals[0] = new Cat;
    animals[1] = new Dog;
    animals[2] = new Cow;

    for (int index = 0; index < animalsNum; ++index)
    {
        animals[index]->Voice();
        delete animals[index];
    }

    return 0;
}